var btnSubmit = document.forms['register-form']['btnSubmit'];
btnSubmit.onclick = function () {
    if (validateForm()) {
        doRegister();
    }
};

function doRegister() {
    var Name = document.forms['register-form']['Name'].value;
    var password = document.forms['register-form']['password'].value;
    var phone = document.forms['register-form']['phone'].value;
    var introduce = document.forms['register-form']['introduce'].value;
    var gender = document.forms['register-form']['gender'].value;
    var email = document.forms['register-form']['email'].value;
    var favorite = document.forms['register-form']['favorite'].value;
    var registerInformation = {
        Name: Name,
        password: password,
        phone: phone,
        introduce: introduce,
        gender: gender,
        email: email,
        favorite: favorite,
    }
    alert('Register success')
}
function validateForm() {
    var isValid = true;
    var isValidName = true;
    var isValidPassword = true;
    var isValidPhone = true;

    var txtName = document.forms['register-form']['Name'];
    var msgName = txtName.nextElementSibling;
    if (txtName.value == null || txtName.value.length == 0 || txtName.value >= 50) {
        msgName.classList.remove('msg-success');
        msgName.classList.add('msg-error');
        msgName.innerHTML = 'Name is required!';
        isValidFirstName = false;
    } else {
        msgFirstName.classList.remove('msg-error');
        msgFirstName.classList.add('msg-success');
        msgFirstName.innerHTML = 'Ok.';
    }

    var pwdPassword = document.forms['register-form']['password'];
    var msgPassword = pwdPassword.nextElementSibling;
    if (pwdPassword.value == null || pwdPassword.value.length == 0) {
        msgPassword.classList.remove('msg-success');
        msgPassword.classList.add('msg-error');
        msgPassword.innerHTML = 'Password is required!';
        isValidPassword = false;
    } else {
        msgPassword.classList.remove('msg-error');
        msgPassword.classList.add('msg-success');
        msgPassword.innerHTML = 'Ok.';
    }
    var pwdphone = document.forms['register-form']['password'];
    var msgphone = pwdphone.nextElementSibling;
    if (pwdphone.value == null || pwdphone.value.length == 0) {
        msgphone.classList.remove('msg-success');
        msgphone.classList.add('msg-error');
        msgphone.innerHTML = 'Phone is required!';
        isValidPhone = false;
    } else {
        msgphone.classList.remove('msg-error');
        msgphone.classList.add('msg-success');
        msgphone.innerHTML = 'Ok.';
    }
    isValid = isValidName && isValidPassword && isValidPhone;
    return isValid;
}
